<?php

use App\Http\Controllers\FilmsController;
use App\Http\Controllers\FilmPlanetsController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// TODO normally if i had api/non-api controllers I might separate them
// TODO out in the HTTP/Controllers by having api/ folder
Route::get('/films', [FilmsController::class, 'index'])->name('films.index');
Route::get('/films/{id}', [FilmsController::class, 'show'])->name('films.index');
Route::get('/films/{id}/planets', [FilmPlanetsController::class, 'index'])->name('films.index');
