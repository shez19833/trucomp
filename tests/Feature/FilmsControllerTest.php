<?php

namespace Tests\Feature;

use Tests\TestCase;

class FilmsControllerTest extends TestCase
{
    public function test_i_can_get_all_films()
    {
        $this->get('api/films')
            ->assertJsonStructure(['data' => ['*' => [
                'title',
                'id',
                'episode_id',
            ]]])
            ->assertJsonFragment([
                'title' => "A New Hope",
                'episode_id' => 4,
                'director' => 'George Lucas',
            ]);
    }

    public function test_i_can_get_a_selected_film()
    {
        $this->get('api/films/5')
            ->assertJsonFragment([
                'title' => "Attack of the Clones",
                'episode_id' => 2,
                'director' => 'George Lucas',
            ]);
    }

    public function test_i_get_a_404_message_if_film_not_found()
    {
        $this->get('api/films/55')
            ->assertJsonFragment([
                'message' => "Sorry, Resource cannot be found",
            ]);
    }
}
