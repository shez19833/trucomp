<?php

namespace Tests\Feature;

use Tests\TestCase;

class FilmPlanetsControllerTest extends TestCase
{
    public function test_i_can_get_all_planets_for_a_selected_film()
    {
        $this->get('api/films/1/planets')
            ->assertJsonStructure(['data' => ['*' => [
                "name",
                "rotation_period",
                "orbital_period",
                "diameter",
            ]]])
            ->assertJsonFragment([
                "name" => "Alderaan",
                "rotation_period" => 24,
                "orbital_period" =>  364,
                "diameter" => 12500,
            ])->assertJsonFragment([
                "name" => "Yavin IV",
                "rotation_period" => 24,
                "orbital_period" => 4818,
                "diameter" => 10200,
            ]);
    }

    public function test_i_get_empty_array_if_planets_not_found_for_a_selected_film()
    {
        $this->get('api/films/55/planets')
            ->assertJson(['data' => []]);
    }
}
