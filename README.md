##  TruComp Tech Test

Test to create some endpoints

GET /films
GET /films/{id}
GET /films/{id}/planets


## Setup

* run `composer install`
* run `cp .env.example .env`
* run `cp films.json storage/app/films.json`
* run `cp planets.json storage/app/planets.json`
* run `php artisan serve` 
* navigate to `api/films`, `api/films/1` or `api/films/1/planets` using the url given by serve command

### Optional

* run `touch database/database.sqlite`
* run `php artisan migrate --seed`
* in AppServiceProvider, comment the JSONFilmsRepository (line 32) and uncomment line 35 DBFilmsRepository. Your routes for films should still give same data but are using database

## Tests

* run `vendor/bin/phpunit`

## Considerations, Things to do

* I only did DB for Films, Planet would follow similar fashion
* I used api.php routes file as it was an api. 
* Add Caching 
* Pagination for index methods
* Authentication for api

