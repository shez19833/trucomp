<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Film;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Film::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'episode_id' => random_int(1, 99),
        'opening_crawl' => $faker->sentences(3, true),
        'director_id' => function() {
            return factory(Director::class)->create()->id;
        }
    ];
});
