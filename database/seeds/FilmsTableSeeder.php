<?php

use App\Models\Film;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $films = json_decode(Storage::get('films.json'));

        foreach ($films as $film) {
            $director = \App\Models\Director::firstOrCreate([
                'name' => $film->director
            ]);

            Film::create([
                'title' => $film->title,
                'episode_id' => $film->episode_id,
                'opening_crawl' => $film->opening_crawl,
                'director_id' => $director->id,
            ]);
        }
    }
}
