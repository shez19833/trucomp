<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Director extends Model
{
    protected $fillable = ['name'];

    public $timestamps = false;

    // TODO quick thing because i normalised the DB design but in json its not
    public function __toString()
    {
        return $this->name;
    }
}
