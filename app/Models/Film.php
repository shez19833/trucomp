<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $fillable = ['title', 'episode_id', 'opening_crawl', 'director_id'];

    protected $primaryKey = 'film_id';

    public function director()
    {
        return $this->belongsTo(Director::class);
    }
}
