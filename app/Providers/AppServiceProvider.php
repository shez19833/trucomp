<?php

namespace App\Providers;

use App\Repositories\Database\FilmsRepository as DBFilmsRepository;
use App\Repositories\FilmsRepositoryInterface;
use App\Repositories\JSON\FilmsRepository as JSONFilmsRepository;
use App\Repositories\JSON\PlanetsRepository as JSONPlanetsRepository;
use App\Repositories\PlanetsRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->bind(FilmsRepositoryInterface::class, JSONFilmsRepository::class);
        $this->app->bind(PlanetsRepositoryInterface::class, JSONPlanetsRepository::class);

//        $this->app->bind(FilmsRepositoryInterface::class, DBFilmsRepository::class);
    }
}
