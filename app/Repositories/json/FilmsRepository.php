<?php

namespace App\Repositories\JSON;

use App\Repositories\FilmsRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class FilmsRepository implements FilmsRepositoryInterface
{
    public function all() : Collection
    {
        return collect(json_decode(Storage::get('films.json')));
    }

    public function findById(int $filmId) : object
    {
        $films = collect(json_decode(Storage::get('films.json')));

        return $films->where('film_id', $filmId)->first() ?? (object)[];
    }
}
