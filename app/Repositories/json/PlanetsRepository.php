<?php

namespace App\Repositories\JSON;

use App\Repositories\FilmsRepositoryInterface;
use App\Repositories\PlanetsRepositoryInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class PlanetsRepository implements PlanetsRepositoryInterface
{
    public function all() : Collection
    {
        return collect(json_decode(Storage::get('planets.json')));
    }

    public function findById(int $planetId) : object
    {
        $planets = collect(json_decode(Storage::get('planets.json')));

        return $planets->where('planet_id', $planetId)->first() ?? (object)[];
    }

    public function getPlanetsForFilmWithId($filmId): Collection
    {
        $planets = collect(json_decode(Storage::get('planets.json')));

        return $planets->filter(function ($planet) use ($filmId){
            return in_array($filmId, $planet->films);
        });
    }
}
