<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

interface PlanetsRepositoryInterface extends RepositoryInterface
{
    public function getPlanetsForFilmWithId($filmId) : Collection;
}
