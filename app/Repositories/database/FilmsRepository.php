<?php

namespace App\Repositories\Database;

use App\Models\Film;
use App\Repositories\FilmsRepositoryInterface;
use Illuminate\Support\Collection;

class FilmsRepository implements FilmsRepositoryInterface
{
    public function all() : Collection
    {
        return Film::with('director')->get();
    }

    public function findById(int $filmId) : object
    {
       return Film::with('director')->find($filmId);
    }
}
