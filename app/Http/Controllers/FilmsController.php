<?php

namespace App\Http\Controllers;

use App\Http\Resources\FilmsResource;
use App\Repositories\FilmsRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FilmsController extends Controller
{
    public function index(FilmsRepositoryInterface $filmsRepository) : AnonymousResourceCollection
    {
        $films = $filmsRepository->all();

        return FilmsResource::collection($films);
    }

    public function show(FilmsRepositoryInterface $filmsRepository, int $filmId)
    {
        $film = $filmsRepository->findById($filmId);

        if (!isset($film->title)) {
            return $this->responseNotFound();
        }

        return new FilmsResource($film);
    }
}
