<?php

namespace App\Http\Controllers;

use App\Http\Resources\PlanetsResource;
use App\Repositories\FilmsRepositoryInterface;
use App\Repositories\PlanetsRepositoryInterface;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class FilmPlanetsController extends Controller
{
    public function index(PlanetsRepositoryInterface $planetsRepository, int $filmId) : AnonymousResourceCollection
    {
        $planets = $planetsRepository->getPlanetsForFilmWithId($filmId);

        return PlanetsResource::collection($planets);
    }
}
