<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanetsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'rotation_period' => (int) $this->rotation_period,
            'orbital_period' => (int) $this->orbital_period,
            'diameter' => (int) $this->diameter,
        ];
    }
}
