<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FilmsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => (string) $this->title,
            'id' => (int) $this->film_id,
            'episode_id' => (int) $this->episode_id,
            'opening_crawl' => (string) $this->opening_crawl,
            'director' => (string) $this->director,
        ];
    }
}
